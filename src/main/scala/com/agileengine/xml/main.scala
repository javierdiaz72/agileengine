package com.agileengine.xml

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element, Attribute}

import scala.util.{Failure, Success, Try}
import scala.collection.JavaConverters._

object main extends App with LazyLogging {

  type ScoredFeature = (String, String)
  
  case class ElementScore(path: String, score: Int)

  private val CHARSET_NAME = "utf8"

  if (args.length < 3) {
    logger.error("Usage: <input_origin_file_path> <button-id> <input_other_sample_file_path>")
    System.exit(1)
  }

  println(getBestScoredPath(args(0), args(1), args(2)).getOrElse("No best score found"))

  def getBestScoredPath(originFilePath: String, targetElementId: String, diffFilePath: String): Option[String] = {
    logger.trace(s"originFilePath: $originFilePath, targetElementId: $targetElementId, diffFilePath: $diffFilePath")
    for {
      originDocument <- getDocument(originFilePath)
      diffFileDocument <- getDocument(diffFilePath)
      val targetElement = originDocument.getElementById(targetElementId)
      val targetScoredFeatures = getScoredFeatures(targetElement)
      bestScored <- diffFileDocument.getAllElements().asScala.toList
        .map(getElementScore(_, targetScoredFeatures))
        .sortWith(_.score > _.score)
        .headOption

    } yield bestScored.path
  }

  def getScoredFeatures(element: Element): Set[ScoredFeature] = {
    val result = element.attributes().asList().asScala.toSet
      .map((attr: Attribute) => (attr.getKey, attr.getValue))
    result + (("html", element.html))
  }

  def getElementScore(element: Element, targetScoredFeatures: Set[ScoredFeature]): ElementScore = {
    val scoredFeatures = getScoredFeatures(element)
    logger.trace(s"scoredFeatures: $scoredFeatures")
    ElementScore(element.cssSelector, scoredFeatures.intersect(targetScoredFeatures).size)
  }

  def getDocument(htmlFileName: String): Option[Document] =
    Try {
      val htmlFile = new File(htmlFileName)
      Jsoup.parse(htmlFile, CHARSET_NAME, htmlFile.getAbsolutePath)
    } match {
      case Success(document) => { logger.trace(s"Document parsed!"); Some(document) }
      case Failure(ex) => { logger.error(s"Error ocurred. " + ex); None }
    }
}
