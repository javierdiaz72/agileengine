# AgileEngine Back-End Scala Test

To run sample 1:

$ sbt
> run ./src/main/resources/samples/sample-0-origin.html make-everything-ok-button ./src/main/resources/samples/sample-1-evil-gemini.html

Output: #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > a.btn.btn-success

To run sample 2

$ sbt
> run ./src/main/resources/samples/sample-0-origin.html make-everything-ok-button ./src/main/resources/samples/sample-2-container-and-clone.html

Output: #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > div.some-container > a.btn.test-link-ok

To run sample 3

$ sbt
> run ./src/main/resources/samples/sample-0-origin.html make-everything-ok-button ./src/main/resources/samples/sample-3-the-escape.html

Output: #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success

To run sample 4

$ sbt
> run ./src/main/resources/samples/sample-0-origin.html make-everything-ok-button ./src/main/resources/samples/sample-4-the-mash.html

Output: #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success

